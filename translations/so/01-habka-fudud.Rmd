# Habka Fudud

Ujeedada buugaan waa inuu ku siiyo maskax cusub. Si ka duwan habka joojinta oo caadiga ah - halkaas oo aad ka bilowdo adigoo dareemaysid inaad fuulayso Mount Everest oo aad ku qaadato dhowrka toddobaad oo soo socda ee damaca porn iyo dareenka wax-la'aannimada - habkaan wuxuu kugu bilaabaa dareen farxadeed, sida haddii lagaa daaweeyay cudur xun. Inta wixii ka dambeeya, intaad nolosha sii marayso, waxaad dib u eegi doontaa muddadaan adigoo la yaabban sida oo aad weligaaga porn u isticmaashay markii hore. Waxaad ka xumaan doontaa isticmaalayaasha porn halkii oo aad ka masayri lahayd.

Haddii aadan ahayn qof balwad lahayn (kuwa u akhrinayaan lamaanayaashooda dartood), qof oo mar hore joojiiyay isticmaalka porn (ama qof oo ku jira muddada ka-hortagga inta lagu jiro "porn-qaadashada la nidaamay"), waa lagma maarmaan in la sii waddo isticmaalka porn ilaa aad si buuxda u dhammayso akhrinta buugga. Tani waxay u muuqan kartaa inay tahay mid oo iska hor imaanaysa, iyo tilmaantaan in siigayashada la sii wado waxay keentaa diidmada ugu badan ee akhristayaasha, laakiin markaad sii akhrido, damacaaga inaad porn isticmaasha ayaa si tartiib tartiib u yaraan doona. **Tilmaantaan si halis u qaado: Isku dayga inaad goor hore joojiso faa'iido kuuma yeelayso.**

Qaar badan ma dhammeeyaan buuga waayo waxay dareemaan inay ku qasban yihiin inay wax ka tanaasulaan, qaar oo xataa si ula kac ah u akhrinayaan hal xariiq maalintii si ay dib ugu dhigaan 'dhacdada xun.' Sidaan u fiiri: Maxaa kaa khasaaraaya? Haddii aadan joojinin ee dhammaadka buuga, xaaladdaada sidii ay ahaan jirtay kama sii xumaanayso. Waa Pascal's Wager qeexitaan ahaan, sharad oo la qaato adigoosan haysanin wax oo kaa khasaaraya oo aad fursadaha sare u haysato ee faa'iidooyinka waaweyn.

Laakiin haa, haddii aadan porn daawanin dhowr maalmood ama dhowr toddobaadyo, oo laakin aadan hubin haddii aad tahay isticmaale porn, isticmaale hore, ama qof oo isticmaale ahayn, ha isticmaalin porn si aad u siigeysato. Dhab ahaantii, waaba tahay qof oo isticmaale ahayn, laakiin waa inaan u ogolaano in maskaxdaada ay jirkaaga la qabsato. Dhammaadka buuga, waxaad noqon doontaa qof faraxsan oo isticmaale ahayn. EasyPeasy waa lidka hababka [kale] oo caadiga ah, halkaas uu mid ka mid ah liis garaynaya khasaaraha porn oo uu leeyahay:
  
*"Haddii aan joogi karo waqti dheer oo ku filan anigoo porn la'aan, ugu dambeyntii damaca wuu bixi doonaa waxaana mar kale ku raaxaysan karaa noloshayda, anigoo xor ka ah addoonsiga porn."*

Tani waa habka macquulka oo loo sameeyo, iyadoo maalin walba kumanaan joojianayaan iyagoo habkaan isticmaalayaan. Si kastaba ha ahaatee, way adagtahay in habkaan lagu guuleysto waayo sababaha soo socda:

**Joojinta PMO ma aha dhibaatada dhabta ah.**
Mar walba oo aad dhammeeyso fadhigaaga porn, waad joojisay isticmaalkeeda. Waxaa laga yaabaa inaad haysato sababo xoog leh maalinta ugu horreysa ee porn qaadashadaada nidaamsan oo "hal mar afartii malmood" si aad u dhahdo *"Ma doonayo inaan isticmaalo porn, ama xataa inaan siigeysto mar dambe."* Dadka oo dhan way sameeyaan, sababahooduna way ka awood badan yihiin wax kasta oo aad qiyaasi karto. Dhibaatada dhabta ah waa maalinta labaad, maalinta tobnaad, ama maalinta toban kumnaad, halkaas oo mid ka mid ah daqiiqadahaaga daciifka ah aad rabto inaad "hal mar fiiriso," oo aad ka dibna rabto fiirin kale, oo aad si lama filaan ah u noqatid balwad mar kale.

**Ka warqabka khataraha caafimaadka waxay abuurtaa cabsi badan, taas oo aad u adkeyneysa joojinta.** 
U sheeg isticmaale inay ragnimadiisa baaba'inayso, waxa ugu horreeya ee uu sameyn doono waa inuu shay soo qabsado si uu u kiciyo dopamine-kiisa: sigaarka, khamriga, ama xataa kicinta browserka si uu porn u raadiyo.

**Sababaha joojinta porn oo dhan waxay dhab ahaantii joojinta adkeeynayaan.**
Sababtuna waa laba sababood. Sababta kowaad waa inay abuurayaan dhalanteedka in nalagu qasbayo inaan iska dhaafno 'saaxiibkeena yar' ama taageerteena, ama ku-raaxaysigeena (si kasta uu isticmaalaha u arko). Marka labaad, "dabool" aya noqonaysaa. Uma siigeysano sababaha ay tahay inaan joojino. Su'aasha dhabta ah waxay tahay, maxaan u rabnaa ama aan ugu baahanahay inaan isticmaalno?

Iyadoo EasyPeasy ah, waxaan (bilowda) iska dhego-tirnaa sababaha oo aan u jeclaan lahayn inaan joojino, waxaana si toos ah u wajahnaa dhibaatada porn, waxaanan nafteena weydiinaa su'aalaha soo socda:

1.  Porn maxay ii qabanaysaa?

2.  Runtii ma ku raaxaystaa?

3.  Runtii ma u baahanahay inaan noloshayda dhex maro anigoo baabi'inaya maskaxdayda iyo jirkayga?

Runta quruxda badan waa in porn ay *gabi ahaan* waxba kuu qabanaynin. Aan si cad u caddeeyo, ma aha in faa'iido darada isticmaasha ay ka miisaan badan yihiin faa'iidooyinka, laakin waa inay **gabi ahaanba** faa'iidooyin ku jirin daawashada porn.

Isticmaalayaasha intooda badan waxay u arkaan inay lagama maarmaan tahay inay caqli-geliyaan sababta oo ay porn u isticmaalaan, laakiin sababaha ay la yimaadaan way khaldan yihiin wayna dhalanteed yihiin.

Kowdi, waxaan meesha ka saaraynaa aminsanaantaan oo khaldan iyo kuwaan oo dhalanteedka ah. Dhab ahaantii, waxaad mar dhow ogaan doontaa inaysan jirin wax oo aad tanaasusho. Intaa waxaa dheer, waxaa jira faa'iidooyin aad u fiican oo togan oo laga helaayo ahaanshaha qof oo PMO sameynin, iyagoo wanaaga iyo farxadada ay yihiin laba oo ka mid ah faa'iidooyinkaan. Markaan dhalanteedka in nolosheena ay weligeeda noqonaynin mid oo lagu raaxaysan karo iyadoo porn la'aan iska saarno -- anagoo garwaaqsano in nolosha aysan la'aanteed kaliya ahayn mid oo lagu raaxaysan karo oo laakin ay raxaysta sidoo kale tahay mid oo si xad lahayn uga sii raaxaysan -- iyo markaan dareenka in wax lagaa qaaday ama in wax ay ka maqnaanayso lagaa ciribtiro, waxaan dib ugu noqon doonaa inaan dib u eegno kor u kaca fayoobaanta iyo farxadda -- iyo daraasiinada sabab oo kale ee joojinta porn. Garashad kuwaan waxay kuu noqon doonaan caawimo dheeraad oo wanaagsan si ay kaaga caawiso inaad gaarto waxa oo aad dhab ahaantii u rabto: raaxada noloshaada adigoo xor ka ah addoonsiga balwadda porn!
